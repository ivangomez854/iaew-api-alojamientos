import * as dotenv from "dotenv";
import { connectDB, initializeData } from "./database";
import { AppRoutes } from "./src/infrastructure/routes";
const bodyParser = require("body-parser");
const { auth } = require("express-oauth2-jwt-bearer");

const express = require("express");
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const app = express();
// configuro las variables de entorno
dotenv.config();

const port = process.env.PORT || 3000;
//@ts-ignore
app.use(bodyParser.json());

const jwtCheck = auth({
  audience: "https://api.example.com/alojamientos",
  issuerBaseURL: "https://dev-ktiy20mxl6ragjzp.us.auth0.com/",
  tokenSigningAlg: "RS256",
});

export { jwtCheck };

const excludeFromAuth = (req, res, next) => {
  if (req.url === "/api-docs") {
    return next();
  }
  jwtCheck(req, res, next); // Aplicar el middleware de autenticación para otras rutas
};

app.get("/authorized", function (req, res) {
  res.send("Secured Resource");
});

// Configuración de Swagger
// const swaggerOptions = {
//   swaggerDefinition: {
//     info: {
//       title: "Gestión de Alojamientos",
//       description:
//         "Administrar información sobre alojamientos, disponibilidad, precios, y reservas.",
//       version: "1.0.0",
//     },
//   },
//   apis: ["./src/swagger/swagger.yaml"],
// };

// const swaggerSpec = swaggerJSDoc(swaggerOptions);

// app.use("/api-docs", swaggerUi.serve);
// app.get("/api-docs", swaggerUi.setup("./src/swagger/swagger.yaml"));

const swaggerDocument = YAML.load("./src/swagger/swagger.yaml");

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(excludeFromAuth);

AppRoutes.forEach((route) => {
  app.use(route.path, route.action);
});

export const startServer = async () => {
  await app.listen(port, () => {
    console.log(`Servidor escuchando en el puerto ${port}`);
  });
};

(async () => {
  await connectDB();
  await initializeData();
  await startServer();
})();
