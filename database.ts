import mongoose from 'mongoose';
import {alojamientosMock} from "./src/domain/mock/mock-alojamientos";
import {AlojamientoModel} from "./src/domain/dto-mongo/AlojamientoModel";
import {ReservaAlojamientoModel} from "./src/domain/dto-mongo/ReservaAlojamientoModel";
import {reservasAlojamientoMock} from "./src/domain/mock/mock_reservaAlojamiento";

export const connectDB = async () => {
    await mongoose.connect(`${process.env.URL_BD}`)
        .then(db => console.log('DB is conected to', db.connection?.name))
        .catch(err => console.error(err));
    mongoose.set('debug', true);
}


export const initializeData = async () => {
    const mockAlojamientos = [...alojamientosMock];
    const mockReservas = [...reservasAlojamientoMock];

    try {
        // Elimina todos los documentos existentes antes de insertar nuevos datos
        //@ts-ignore
        await AlojamientoModel.deleteMany({});
        //@ts-ignore
        await ReservaAlojamientoModel.deleteMany({});

        // Inserta los datos mockeados
        //@ts-ignore
        await AlojamientoModel.insertMany(mockAlojamientos);
        //@ts-ignore
        await ReservaAlojamientoModel.insertMany(mockReservas);

        console.log('Datos inicializados con éxito.');
    } catch (error) {
        console.error('Error al inicializar datos:', error);
    }
};