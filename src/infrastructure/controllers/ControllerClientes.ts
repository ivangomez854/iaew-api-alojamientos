import {Request, Response} from "express";
import {HttpCodes} from "./HttpCodes";
import {ClientesService} from "../../aplication/ClientesService";

const _clientesService = ClientesService.getInstance();

async function obtenerReservasCliente(request: Request, response: Response) {
    const idCliente = +request.params.id;

    _clientesService.getReservasPorCliente(idCliente)
        .then(reservas => {
            return response.status(HttpCodes.OK).json(reservas)
        })
        .catch(error => {
            return response.status(HttpCodes.NOT_FOUND).json(error.message);
        });
}

export const ClientesController = {
    obtenerReservasCliente
};