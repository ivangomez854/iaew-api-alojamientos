import {Request, Response} from "express";
import {HttpCodes} from "./HttpCodes";
import {ReservaAlojamientosService} from "../../aplication/ReservaAlojamientoService";

// Creo la instancia de ReservaAlojamientoService
const _reservaAlojamientosServices = ReservaAlojamientosService.getInstance();

async function obtenerReservasAlojamiento(request: Request, response: Response) {
    _reservaAlojamientosServices.getReservasAlojamientos()
        .then(reservaAlojamiento => {
            return response.status(HttpCodes.OK).json(reservaAlojamiento);
        })
        .catch(error => {
        return response.status(HttpCodes.NOT_FOUND).json(error.message);
    });
}

async function nuevaReservaAlojamiento(request: Request, response: Response) {
    _reservaAlojamientosServices.addReservaAlojamiento(request.body)
         .then(reserva => {
             return response.status(HttpCodes.OK).json(reserva)
         })
         .catch(error => {
             return response.status(HttpCodes.NOT_FOUND).json(error.message);
         });
}

export const ReservaAlojamientosController = {
    obtenerReservasAlojamiento,
    nuevaReservaAlojamiento,
};