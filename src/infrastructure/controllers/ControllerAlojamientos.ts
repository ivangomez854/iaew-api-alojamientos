import {Request, Response} from "express";
import {HttpCodes} from "./HttpCodes";
import {AlojamientosService} from "../../aplication/AlojamientosService";
import {IAlojamientosService} from "../../aplication/interfaces/IAlojamientosService";

// Creo la instancia de alojamientosServices
const _alojamientosServices: IAlojamientosService = AlojamientosService.getInstance();

async function obtenerAlojamientos(request: Request, response: Response) {
    _alojamientosServices.getAlojamientos()
        .then(alojamientos => {
            return response.status(HttpCodes.OK).json(alojamientos);
        })
        .catch(error => {
        return response.status(HttpCodes.NOT_FOUND).json(error.message);
    });

}

async function obtenerAlojamientoPorId(request: Request, response: Response) {
    const idAlojamiento = +request.params.id;

    _alojamientosServices.getAlojamientoPorId(idAlojamiento)
         .then(alojamiento => {
             return response.status(HttpCodes.OK).json(alojamiento)
         })
         .catch(error => {
             return response.status(HttpCodes.NOT_FOUND).json(error.message);
         });
}

async function nuevoAlojamiento(request: Request, response: Response) {
    _alojamientosServices.addAlojamiento(request.body)
         .then(alojamiento => {
             return response.status(HttpCodes.OK).json(alojamiento)
         })
         .catch(error => {
             return response.status(HttpCodes.NOT_FOUND).json(error.message);
         });
}

async function actualizarAlojamiento(request: Request, response: Response) {
    const idAlojamiento = +request.params.id;
    const datosAlojamiento = request.body;

    _alojamientosServices.updateAlojamiento(idAlojamiento, datosAlojamiento)
         .then(alojamiento => {
             return response.status(HttpCodes.OK).json(alojamiento)
         })
         .catch(error => {
             return response.status(HttpCodes.NOT_FOUND).json(error.message);
         });
}

async function eliminarAlojamiento(request: Request, response: Response) {
    const idAlojamiento = +request.params.id;

    _alojamientosServices.deleteAlojamiento(idAlojamiento)
         .then(alojamiento => {
             return response.status(HttpCodes.OK).json(alojamiento)
         })
         .catch(error => {
             return response.status(HttpCodes.NOT_FOUND).json(error.message);
         });
}


export const AlojamientosController = {
    obtenerAlojamientos,
    obtenerAlojamientoPorId,
    nuevoAlojamiento,
    actualizarAlojamiento,
    eliminarAlojamiento
};