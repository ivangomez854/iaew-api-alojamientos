import {IAlojamientoRepository} from "./interfaces/IAlojamientoRepository";
import {Alojamiento} from "../../domain/models/Alojamiento";
import {AlojamientoModel} from "../../domain/dto-mongo/AlojamientoModel";

export class AlojamientoRepositoryMongo implements IAlojamientoRepository {

    private static instancia: AlojamientoRepositoryMongo;

    private constructor() {
    }

    public static getInstance(): AlojamientoRepositoryMongo {
        return this.instancia ? this.instancia : new AlojamientoRepositoryMongo();
    }

    async addAlojamiento(alojamiento: Alojamiento): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                await AlojamientoModel.create(alojamiento);
                console.log('CREATE', alojamiento);
                resolve(true);
            } catch (error) {
                console.error('Error al agregar alojamiento:', error);
                reject(error);
            }
        });
    }

    async deleteAlojamiento(id: number): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                const result = await AlojamientoModel.deleteOne({ id });
                console.log('DELETE', 'ID', id);
                //@ts-ignore
                resolve(result.count === 1);
            } catch (error) {
                console.error('Error al eliminar alojamiento:', error);
                reject(error);
            }
        });
    }

    async getAlojamientoPorId(id: number): Promise<Alojamiento> {
        return new Promise<Alojamiento>(async (resolve, reject) => {
            try {
                const alojamiento = await AlojamientoModel.findOne({ id });
                console.log('GET POR ID', id);
                //@ts-ignore
                resolve(alojamiento);
            } catch (error) {
                console.error('Error al obtener alojamiento por ID:', error);
                reject(error);
            }
        });
    }

    async getAlojamientos(): Promise<Alojamiento[]> {
        return new Promise<Alojamiento[]>(async (resolve, reject) => {
            try {
                const alojamientos = await AlojamientoModel.find();
                console.log('GET ALOJAMIENTOS');
                //@ts-ignore
                resolve(alojamientos.map((alojamiento) => alojamiento.toObject()));
            } catch (error) {
                console.error('Error al obtener alojamientos:', error);
                reject(error)
            }
        });
    }

    async updateAlojamiento(id: number, datosAlojamiento: Alojamiento): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                const result = await AlojamientoModel.updateOne({ id }, datosAlojamiento);
                console.log('UPDATE', 'ID', id, 'PAYLOAD', datosAlojamiento);
                //@ts-ignore
                resolve(result.modifiedCount === 1);
            } catch (error) {
                console.error('Error al actualizar alojamiento:', error);
                reject(error);
            }
        });
    }
}