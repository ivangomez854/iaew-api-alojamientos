import {IAlojamientoRepository} from "./interfaces/IAlojamientoRepository";
import {Alojamiento} from "../../domain/models/Alojamiento";
import {alojamientosMock} from "../../domain/mock/mock-alojamientos";

export class AlojamientoRepositoryLocal implements IAlojamientoRepository {
    private static instancia: AlojamientoRepositoryLocal;
    private ultimoId: number;
    private listaAlojamientos: Alojamiento[];

    private constructor() {
        this.listaAlojamientos = [...alojamientosMock];
        this.ultimoId = this.getLastId();
    }

    public static getInstance(): AlojamientoRepositoryLocal {
        return this.instancia ? this.instancia : new AlojamientoRepositoryLocal();
    }

    private getLastId(): number {
        let ultimoId = 0;
        this.listaAlojamientos.forEach(alojamiento => {
            ultimoId = alojamiento.id > ultimoId ? alojamiento.id : ultimoId;
        });
        return ultimoId;
    }

    public getAlojamientos(): Promise<Alojamiento[]> {
        return new Promise((resolve, reject) => {
            resolve(this.listaAlojamientos);
        });
    }

    public getAlojamientoPorId(id: number): Promise<Alojamiento> {
        return new Promise<Alojamiento>((resolve, reject) => {
            const alojamiento = this.listaAlojamientos.find(alojamiento => alojamiento.id === id);

            if (alojamiento) {
                resolve(alojamiento);
            } else {
                reject(new Error('No se encontró el alojamiento con el id solicitado.'));
            }
        });
    }

    public addAlojamiento(alojamiento: Alojamiento): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            try {
                const nuevo = new Alojamiento(
                    ++this.ultimoId,
                    alojamiento.nombre,
                    alojamiento.ubicacion,
                    alojamiento.tipo,
                    alojamiento.precioPorNoche,
                    alojamiento.capacidad,
                    alojamiento.facilidades,
                    alojamiento.idProveedor
                );
                this.listaAlojamientos.push(nuevo);
                resolve(true);
            } catch (err) {
                console.log(err);
                reject(new Error('Error al intentar crear alojamiento.'));
            }
        });
    }

    public updateAlojamiento(id: number, datosAlojamiento: Alojamiento): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const alojamiento = this.listaAlojamientos.find(alojamiento => alojamiento.id === id);
            if (alojamiento) {
                alojamiento.nombre = datosAlojamiento.nombre || alojamiento.nombre;
                alojamiento.ubicacion = datosAlojamiento.ubicacion || alojamiento.ubicacion;
                alojamiento.tipo = datosAlojamiento.tipo || alojamiento.tipo;
                alojamiento.precioPorNoche = datosAlojamiento.precioPorNoche || alojamiento.precioPorNoche;
                alojamiento.capacidad = datosAlojamiento.capacidad || alojamiento.capacidad;
                alojamiento.capacidad = datosAlojamiento.capacidad || alojamiento.capacidad;
                alojamiento.facilidades = datosAlojamiento.facilidades || alojamiento.facilidades;
                alojamiento.idProveedor = datosAlojamiento.idProveedor || alojamiento.idProveedor;
                resolve(true);
            } else {
                reject(new Error('No se encontró el alojamiento con el id solicitado.'));
            }
        });
    }

    public deleteAlojamiento(id: number): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const indice = this.listaAlojamientos.findIndex(est => est.id === id);
            if (indice != -1) {
                this.listaAlojamientos.splice(indice, 1);
                resolve(true);
            } else {
                reject(new Error('No se encontró el alojamiento con el id solicitado.'));
            }
        });
    }

}