import {ReservaAlojamiento} from "../../../domain/models/ReservaAlojamiento";

export interface IReservasRepository {

getReservasAlojamientos(): Promise<ReservaAlojamiento[]>;

addReservaAlojamiento(reservaAlojamiento: ReservaAlojamiento): Promise<boolean>;
}