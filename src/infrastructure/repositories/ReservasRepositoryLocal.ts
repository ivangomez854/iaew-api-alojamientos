import {IReservasRepository} from "./interfaces/IReservasRepository";
import {ReservaAlojamiento} from "../../domain/models/ReservaAlojamiento";
import {reservasAlojamientoMock} from "../../domain/mock/mock_reservaAlojamiento";

export class ReservasRepositoryLocal implements IReservasRepository {

    private static instancia: ReservasRepositoryLocal;
    private ultimoId: number;
    private listaReservaAlojamientos: ReservaAlojamiento[];

    private constructor() {
        this.listaReservaAlojamientos = [...reservasAlojamientoMock];
        this.ultimoId = this.getLastId();
    }

    public static getInstance(): ReservasRepositoryLocal {
        return this.instancia ? this.instancia : new ReservasRepositoryLocal();
    }

    private getLastId(): number {
        let ultimoId = 0;
        this.listaReservaAlojamientos.forEach(alojamiento => {
            ultimoId = alojamiento.id > ultimoId ? alojamiento.id : ultimoId;
        });
        return ultimoId;
    }

    public getReservasAlojamientos(): Promise<ReservaAlojamiento[]> {
        return new Promise((resolve, reject) => {
            resolve(this.listaReservaAlojamientos);
        });
    }

    public addReservaAlojamiento(reservaAlojamiento: ReservaAlojamiento): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            try {
                const nuevo = new ReservaAlojamiento(
                    ++this.ultimoId,
                    reservaAlojamiento.alojamientoId,
                    reservaAlojamiento.clienteId,
                    reservaAlojamiento.fechaEntrada,
                    reservaAlojamiento.fechaSalida,
                    reservaAlojamiento.huespedes,
                    reservaAlojamiento.precioTotal,
                    reservaAlojamiento.estado
                );
                this.listaReservaAlojamientos.push(nuevo);
                resolve(true);
            } catch (err) {
                console.log(err);
                reject(new Error('Error al intentar Reservar el alojamiento.'));
            }
        });
    }
}