import {IReservasRepository} from "./interfaces/IReservasRepository";
import {ReservaAlojamiento} from "../../domain/models/ReservaAlojamiento";
import {ReservaAlojamientoModel} from "../../domain/dto-mongo/ReservaAlojamientoModel";

export class ReservasRepositoryMongo implements IReservasRepository {
    private static instancia: ReservasRepositoryMongo;

    private constructor() {
    }

    public static getInstance(): IReservasRepository {
        return this.instancia ? this.instancia : new ReservasRepositoryMongo();
    }

    addReservaAlojamiento(reservaAlojamiento: ReservaAlojamiento): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                await ReservaAlojamientoModel.create(reservaAlojamiento);
                console.log('CREATE', reservaAlojamiento);
                resolve(true);
            } catch (err) {
                console.error('Error', err);
                reject(err);
            }
        });
    }

    getReservasAlojamientos(): Promise<ReservaAlojamiento[]> {
        return new Promise<ReservaAlojamiento[]>(async (resolve, reject) => {
            try {
                const reservas = await ReservaAlojamientoModel.find();
                resolve(reservas)
            } catch (err) {
                console.error('Error', err);
                reject(err);
            }
        });
    }
}