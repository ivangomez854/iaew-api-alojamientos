import { AlojamientosController } from "./controllers/ControllerAlojamientos";
import { ClientesController } from "./controllers/ControllerClientes";
import { ReservaAlojamientosController } from "./controllers/ControllerReservaAlojamiento";

export const AppRoutes = [
  {
    path: "/alojamientos/obtener-alojamientos",
    method: "get",
    action: AlojamientosController.obtenerAlojamientos,
  },
  {
    path: "/alojamientos/obtener-alojamiento/:id",
    method: "get",
    action: AlojamientosController.obtenerAlojamientoPorId,
  },

  {
    path: "/alojamientos/nuevo-alojamiento",
    method: "post",
    action: AlojamientosController.nuevoAlojamiento,
  },
  {
    path: "/alojamientos/actualizar-alojamiento/:id",
    method: "put",
    action: AlojamientosController.actualizarAlojamiento,
  },
  {
    path: "/alojamientos/eliminar-alojamiento/:id",
    method: "delete",
    action: AlojamientosController.eliminarAlojamiento,
  },
  {
    path: "/reservas/obtener-reservas",
    method: "get",
    action: ReservaAlojamientosController.obtenerReservasAlojamiento,
  },
  {
    path: "/reservas/nueva-reserva",
    method: "post",
    action: ReservaAlojamientosController.nuevaReservaAlojamiento,
  },
  {
    path: "/clientes/:id/reservas-alojamientos",
    method: "get",
    action: ClientesController.obtenerReservasCliente,
  },
];
