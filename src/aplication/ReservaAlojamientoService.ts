import {ReservaAlojamiento} from "../domain/models/ReservaAlojamiento";
import {IReservasRepository} from "../infrastructure/repositories/interfaces/IReservasRepository";
import {ReservasRepositoryMongo} from "../infrastructure/repositories/ReservasRepositoryMongo";
import {IReservasService} from "./interfaces/IReservasService";

export class ReservaAlojamientosService implements IReservasService{

    private static instancia: ReservaAlojamientosService;
    private _reservasRepository: IReservasRepository;

    private constructor() {
        this._reservasRepository = ReservasRepositoryMongo.getInstance();
        // this._reservasRepository = ReservasRepositoryLocal.getInstance();
    }

    public static getInstance(): ReservaAlojamientosService {
        return this.instancia ? this.instancia : new ReservaAlojamientosService();
    }

    public getReservasAlojamientos(): Promise<ReservaAlojamiento[]> {
        return new Promise((resolve, reject) => {
            this._reservasRepository.getReservasAlojamientos()
                .then(reservas => resolve(reservas))
                .catch(err => {
                    console.error('Error' + err);
                    reject(err);
                });
        });
    }

    public addReservaAlojamiento(reservaAlojamiento: ReservaAlojamiento): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this._reservasRepository.addReservaAlojamiento(reservaAlojamiento)
                .then(resultado => resolve(resultado))
                .catch(err => {
                    console.error('Error', err);
                    reject(err);
                });
        });
    }
}