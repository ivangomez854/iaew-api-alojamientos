import {ReservaAlojamiento} from "../domain/models/ReservaAlojamiento";
import {ReservaAlojamientosService} from "./ReservaAlojamientoService";

export class ClientesService {

    private static instancia: ClientesService;

    private _reservasService: ReservaAlojamientosService;

    private constructor() {
        this._reservasService = ReservaAlojamientosService.getInstance();
    }

    public static getInstance(): ClientesService {
        return this.instancia ? this.instancia : new ClientesService();
    }

    public async getReservasPorCliente(id: number): Promise<ReservaAlojamiento[]> {
        return new Promise<ReservaAlojamiento[]>(async (resolve, reject) => {
            const reservas = await this._reservasService.getReservasAlojamientos();
            const filtrados = reservas.filter(reserva => reserva.clienteId === id);

            if (filtrados.length) {
                resolve(filtrados);
            } else {
                reject(new Error('No se encontraron reservas para el id de cliente solicitado.'));
            }
        });
    }
}