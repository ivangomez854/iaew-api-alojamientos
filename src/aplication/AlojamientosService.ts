import {Alojamiento} from "../domain/models/Alojamiento";
import {IAlojamientosService} from "./interfaces/IAlojamientosService";
import {IAlojamientoRepository} from "../infrastructure/repositories/interfaces/IAlojamientoRepository";
import {AlojamientoRepositoryLocal} from "../infrastructure/repositories/AlojamientoRepositoryLocal";
import {AlojamientoRepositoryMongo} from "../infrastructure/repositories/AlojamientoRepositoryMongo";

export class AlojamientosService implements IAlojamientosService {

    private static instancia: AlojamientosService;
    private _alojamientosRepository: IAlojamientoRepository;

    private constructor() {
        // this._alojamientosRepository = AlojamientoRepositoryLocal.getInstance();
        this._alojamientosRepository = AlojamientoRepositoryMongo.getInstance();
    }

    public static getInstance(): AlojamientosService {
        return this.instancia ? this.instancia : new AlojamientosService();
    }

    public getAlojamientos(): Promise<Alojamiento[]> {
        return new Promise<Alojamiento[]>((resolve, reject) => {
            this._alojamientosRepository.getAlojamientos()
                .then(alojamientos => {
                    resolve(alojamientos);
                })
                .catch(err => {
                    reject(err)
                });
        });
    }

    public getAlojamientoPorId(id: number): Promise<Alojamiento> {
        return new Promise<Alojamiento>((resolve, reject) => {
            this._alojamientosRepository.getAlojamientoPorId(id)
                .then(alojamientos => {
                    resolve(alojamientos);
                })
                .catch(err => {
                    reject(err)
                });
        });
    }

    public addAlojamiento(alojamiento: Alojamiento): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this._alojamientosRepository.addAlojamiento(alojamiento)
                .then(alojamientos => {
                    resolve(alojamientos);
                })
                .catch(err => {
                    reject(err)
                });
        });
    }

    public updateAlojamiento(id: number, datosAlojamiento: Alojamiento): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this._alojamientosRepository.updateAlojamiento(id, datosAlojamiento)
                .then(alojamientos => {
                    resolve(alojamientos);
                })
                .catch(err => {
                    reject(err)
                });
        });
    }

    public deleteAlojamiento(id: number): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this._alojamientosRepository.deleteAlojamiento(id)
                .then(alojamientos => {
                    resolve(alojamientos);
                })
                .catch(err => {
                    reject(err)
                });
        });
    }
}