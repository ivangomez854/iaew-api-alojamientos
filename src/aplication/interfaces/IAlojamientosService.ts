import {Alojamiento} from "../../domain/models/Alojamiento";

export interface IAlojamientosService {

    getAlojamientos(): Promise<Alojamiento[]>;

    getAlojamientoPorId(id: number): Promise<Alojamiento>;

    addAlojamiento(alojamiento: Alojamiento): Promise<boolean>;

    updateAlojamiento(id: number, datosAlojamiento: Alojamiento): Promise<boolean>;

    deleteAlojamiento(id: number): Promise<boolean>;
}