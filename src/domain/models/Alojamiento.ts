export class Alojamiento {
    id: number;
    nombre: string;
    ubicacion: string;
    tipo: string;
    precioPorNoche: number;
    capacidad: number;
    facilidades: string;
    idProveedor: number;

    constructor(id: number,
                nombre: string,
                ubicacion: string,
                tipo: string,
                precioPorNoche: number,
                capacidad: number,
                facilidades: string,
                idProveedor: number) {

        this.id = id;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        this.tipo = tipo;
        this.precioPorNoche = precioPorNoche;
        this.capacidad = capacidad;
        this.facilidades = facilidades;
        this.idProveedor = idProveedor;
    }
}