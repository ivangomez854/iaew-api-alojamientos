export class ReservaAlojamiento {
    id: number;
    alojamientoId: number;
    clienteId: number;
    fechaEntrada: Date;
    fechaSalida:Date;
    huespedes: number;
    precioTotal: number;
    estado: string;

    constructor(

        id: number,
        alojamientoId: number,
        clienteId: number,
        fechaEntrada: Date,
        fechaSalida:Date,
        huespedes: number,
        precioTotal: number,
        estado: string,)
        {
            this.id = id;
            this.alojamientoId = alojamientoId;
            this.clienteId = clienteId;
            this.fechaEntrada = fechaEntrada;
            this.fechaSalida = fechaSalida;
            this.huespedes = huespedes;
            this.precioTotal = precioTotal;
            this.estado = estado;
        }
}