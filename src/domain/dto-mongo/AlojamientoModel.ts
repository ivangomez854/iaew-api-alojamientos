import mongoose, { Document, Schema } from 'mongoose';
import { Alojamiento } from '../models/Alojamiento';

// Define el esquema de Mongoose para Alojamiento
const alojamientoSchema = new Schema<Alojamiento & Document>({
    id: { type: Number, required: true },
    nombre: { type: String, required: true },
    ubicacion: { type: String, required: true },
    tipo: { type: String, required: true },
    precioPorNoche: { type: Number, required: true },
    capacidad: { type: Number, required: true },
    facilidades: { type: String, required: true },
    idProveedor: { type: Number, required: true },
});

// Crea y exporta el modelo
//@ts-ignore
export const AlojamientoModel = mongoose.model<Alojamiento & Document>('Alojamiento', alojamientoSchema);