import mongoose, {Document, Schema} from 'mongoose';
import {ReservaAlojamiento} from "../models/ReservaAlojamiento";

// Define el esquema de Mongoose para Alojamiento
const reservaAlojamientoSchema = new Schema<ReservaAlojamiento & Document>({
    id: { type: Number, required: false },
    alojamientoId: { type: Number, required: true },
    clienteId: { type: Number, required: true },
    fechaEntrada: { type: Date, required: true },
    fechaSalida: { type: Date, required: false },
    huespedes: { type: Number, required: true },
    precioTotal: { type: Number, required: true },
    estado: { type: String, required: true },
});

// Crea y exporta el modelo
//@ts-ignore
export const ReservaAlojamientoModel = mongoose.model<ReservaAlojamiento & Document>('ReservaAlojamiento', reservaAlojamientoSchema);